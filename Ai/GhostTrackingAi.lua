local lhand = script.Parent:FindFirstChild("LeftHand")
local rhand = script.Parent:FindFirstChild("RightHand")

function findNearestTorso(pos)
	local list = game.Workspace:children()
	local torso = nil
	local dist = 1000
	local temp = nil
	local human = nil
	local temp2 = nil
	for x = 1, #list do
		temp2 = list[x]
		if (temp2.className == "Model") and (temp2 ~= script.Parent) then

			temp = temp2:findFirstChild("UpperTorso")
			human = temp2:findFirstChild("Humanoid")
			if (temp ~= nil) and (human ~= nil) and (human.Health > 0) then
				if (temp.Position - pos).magnitude < dist then
					torso = temp
					dist = (temp.Position - pos).magnitude
				end
			end
		end
	end
	return torso
end


function Hit(part)
	if part then
		local human = part.Parent:FindFirstChild("Humanoid")
	
		if not human then
			human = part.Parent.Parent:FindFirstChild("Humanoid")
		end
		
		if human then
			human:TakeDamage(15)
		end
	end	
end

lhand.Touched:connect(Hit)
rhand.Touched:connect(Hit)



while true do
	wait(0.1)
	
	if script.Parent.Humanoid.Health == 0 then
		script.Parent.Death:Play()
		break
	end
	
	local target = findNearestTorso(script.Parent.UpperTorso.Position)
	if target ~= nil then
		script.Parent.Humanoid:MoveTo(target.Position, target)
	end
end


