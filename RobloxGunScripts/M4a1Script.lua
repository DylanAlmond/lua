local tool = script.Parent
local player = game:GetService("Players").LocalPlayer
local sound = tool.Handle.Shoot
local ammo = 20
local tknAmmo = 2

print(ammo)

tool.Equipped:connect(function(mouse)	
	mouse.Icon = 'rbxassetid://79658449'
	
	mouse.Button1Up:connect(function()
		mouseDown = false
	end)
	
	mouse.Button1Down:connect(function()
		mouseDown = true
		if mouseDown == true then
				sound:Play()
				ammo = ammo - tknAmmo
				print(ammo)
			if ammo ~= 0 then				
				repeat
					wait(.1)				
						
					local ray = Ray.new(tool.Handle.CFrame.p, (mouse.Hit.p - tool.Handle.CFrame.p).unit * 300)
					local part, position = workspace:FindPartOnRay(ray, player.Character, false, true)
					

			
					local beam = Instance.new("Part", workspace)
					beam.BrickColor = BrickColor.new("Bright yellow")
					beam.FormFactor = "Custom"
					beam.Material = "Neon"
					beam.Transparency = 0.25
					beam.Anchored = true
					beam.Locked = true
					beam.CanCollide = false
				
					local distance = (tool.Handle.CFrame.p - position).magnitude
					beam.Size = Vector3.new(0.3, 0.3, distance)
					beam.CFrame = CFrame.new(tool.Handle.CFrame.p, position) * CFrame.new(0, 0, -distance / 2)
				
					game:GetService("Debris"):AddItem(beam, 0.1)
				
					if part then
						local humanoid = part.Parent:FindFirstChild("Humanoid")
		
						if not humanoid then
							humanoid = part.Parent.Parent:FindFirstChild("Humanoid")
						end
			
						if humanoid then
							humanoid:TakeDamage(15)
						end
					end	
				until mouseDown == false
				else
					print("out of ammo")		
				end		
			end	

	end)
end)

tool.Unequipped:connect(function(disable)
	mouseDown = false	
end)